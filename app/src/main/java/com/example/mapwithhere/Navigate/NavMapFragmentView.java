package com.example.mapwithhere.Navigate;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.mapwithhere.R;
import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.guidance.NavigationManager;
import com.here.android.mpa.mapping.AndroidXMapFragment;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.CoreRouter;
import com.here.android.mpa.routing.Route;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;
import com.here.android.mpa.routing.RouteWaypoint;
import com.here.android.mpa.routing.Router;
import com.here.android.mpa.routing.RoutingError;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;

public class NavMapFragmentView {
    private AndroidXMapFragment m_mapFragment;
    private AppCompatActivity m_activity;
    private Button m_naviControlButton;
    private Map m_map;
    private NavigationManager m_navigationManager;
    private GeoBoundingBox m_geoBoundingBox;
    private Route m_route;
    private boolean m_foregroundServiceStarted;
    Address addresss,addresss1;
    Geocoder geocoder, geocoder1;

    public NavMapFragmentView(AppCompatActivity activity) {
        m_activity = activity;
        initMapFragment();
        initNaviControlButton();
    }

    private AndroidXMapFragment getMapFragment() {
        return (AndroidXMapFragment) m_activity.getSupportFragmentManager().findFragmentById(R.id.mapfragment);
    }

    private void initMapFragment() {
        m_mapFragment = getMapFragment();
        if (m_mapFragment != null) {
            m_mapFragment.init(new OnEngineInitListener() {
                @Override
                public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                    if (error == Error.NONE) {
                        showdialouge();
                    } else {
                        new AlertDialog.Builder(m_activity).setMessage("Error : " + error.name() + "\n\n" + error.getDetails())
                                .setTitle(R.string.engine_init_error)
                                .setNegativeButton(android.R.string.cancel,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                m_activity.finish();
                                            }
                                        }).create().show();
                    }
                }
            });
        }
    }

    private void showdialouge() {
        final Dialog findroute=new Dialog(m_activity,R.style.Theme_AppCompat_Light_DialogWhenLarge);
        findroute.setContentView(R.layout.location_address);
        final EditText start_location=findroute.findViewById(R.id.start_address);
        final EditText end_location=findroute.findViewById(R.id.end_address);
        Button findroutebtn=findroute.findViewById(R.id.find_address);
        findroute.show();

        findroutebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (start_location.getText().toString()!=null&&end_location.getText().toString()!=null&&!start_location.getText().toString().isEmpty()&&!end_location.getText().toString().isEmpty()){
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                             geocoder = new Geocoder(m_activity, Locale.getDefault());
                             geocoder1 = new Geocoder(m_activity, Locale.getDefault());
                            String result = null,result1 = null;
                            StringBuilder sb1,sb;
                            try {
                                List<Address> addressList = geocoder.getFromLocationName(start_location.getText().toString().trim(), 1);
                                if (addressList != null && addressList.size() > 0) {
                                    addresss = addressList.get(0);
                                    sb = new StringBuilder();
                                    sb.append(addresss.getLatitude()).append("\n");
                                    sb.append(addresss.getLongitude()).append("\n");
                                    result = sb.toString();
                                    m_map = m_mapFragment.getMap();
                                    m_map.setCenter(new GeoCoordinate(addresss.getLatitude(), addresss.getLongitude()), Map.Animation.NONE);
                                    m_map.setZoomLevel(13.2);
                                    m_navigationManager = NavigationManager.getInstance();
                                }
                                List<Address> addressList1 = geocoder1.getFromLocationName(end_location.getText().toString().trim(), 1);
                                if (addressList1 != null && addressList1.size() > 0) {
                                    addresss1 = addressList1.get(0);
                                    sb1 = new StringBuilder();
                                    sb1.append(addresss1.getLatitude()).append("\n");
                                    sb1.append(addresss1.getLongitude()).append("\n");
                                    result = sb1.toString();
                                }
                            } catch (IOException e) {
                                Log.e(TAG, "Error   ", e);
                            }
                        }
                    };
                    thread.start();
                }
                findroute.dismiss();
            }
        });
    }

    private void createRoute() {

        CoreRouter coreRouter = new CoreRouter();
        RoutePlan routePlan = new RoutePlan();

        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(RouteOptions.TransportMode.CAR);
        routeOptions.setHighwaysAllowed(true);
        routeOptions.setRouteType(RouteOptions.Type.SHORTEST);
        routeOptions.setRouteCount(1);
        routePlan.setRouteOptions(routeOptions);
        RouteWaypoint startPoint = null,destination = null;
        if (addresss!=null) {
            startPoint = new RouteWaypoint(new GeoCoordinate(addresss.getLatitude(), addresss.getLongitude()));
        }else {
            startPoint = new RouteWaypoint(new GeoCoordinate(11.229592, 78.171158));
        }
        if (addresss1!=null) {
            destination = new RouteWaypoint(new GeoCoordinate(addresss1.getLatitude(), addresss1.getLongitude()));
        }else {
            destination=new RouteWaypoint(new GeoCoordinate(11.664325,78.146011));
        }
        routePlan.addWaypoint(startPoint);
        routePlan.addWaypoint(destination);

        coreRouter.calculateRoute(routePlan, new Router.Listener<List<RouteResult>, RoutingError>() {

                    @Override
                    public void onProgress(int i) {
                    }

                    @Override
                    public void onCalculateRouteFinished(List<RouteResult> routeResults, RoutingError routingError) {
                        if (routingError == RoutingError.NONE) {
                            if (routeResults.get(0).getRoute() != null) {

                                try {
                                    m_route = routeResults.get(0).getRoute();
                                    MapRoute mapRoute = new MapRoute(routeResults.get(0).getRoute());
                                    mapRoute.setManeuverNumberVisible(true);
                                    m_map.addMapObject(mapRoute);
                                    m_geoBoundingBox = routeResults.get(0).getRoute().getBoundingBox();
                                    m_map.zoomTo(m_geoBoundingBox, Map.Animation.NONE, Map.MOVE_PRESERVE_ORIENTATION);

                                    startNavigation();
                                }catch(Exception e){
                                    Toast.makeText(m_activity, "Route not found   "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(m_activity, "Error:route results returned is not valid", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            System.out.println("route   "+routingError);
                            Toast.makeText(m_activity, "Error:route calculation returned error code: " + routingError, Toast.LENGTH_LONG).show();

                        }
                    }
                });
    }

    private void initNaviControlButton() {
        m_naviControlButton = (Button) m_activity.findViewById(R.id.naviCtrlButton);
        m_naviControlButton.setText(R.string.start_navi);
        m_naviControlButton.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {

                if (m_route == null) {
                    createRoute();
                } else {
                    m_navigationManager.stop();
                    m_map.zoomTo(m_geoBoundingBox, Map.Animation.NONE, 0f);
                    m_naviControlButton.setText(R.string.start_navi);
                    m_route = null;
                }
            }
        });
    }

    private void startForegroundService() {
        if (!m_foregroundServiceStarted) {
            m_foregroundServiceStarted = true;
            Intent startIntent = new Intent(m_activity, ForegroundService.class);
            startIntent.setAction(ForegroundService.START_ACTION);
            m_activity.getApplicationContext().startService(startIntent);
        }
    }

    private void stopForegroundService() {
        if (m_foregroundServiceStarted) {
            m_foregroundServiceStarted = false;
            Intent stopIntent = new Intent(m_activity, ForegroundService.class);
            stopIntent.setAction(ForegroundService.STOP_ACTION);
            m_activity.getApplicationContext().startService(stopIntent);
        }
    }

    private void startNavigation() {
        m_naviControlButton.setText(R.string.stop_navi);
        m_navigationManager.setMap(m_map);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(m_activity);
        alertDialogBuilder.setTitle("Navigation");
        alertDialogBuilder.setMessage("Choose Mode");
        alertDialogBuilder.setNegativeButton("Navigation",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                m_navigationManager.startNavigation(m_route);
                m_map.setTilt(60);
                startForegroundService();
            };
        });
        alertDialogBuilder.setPositiveButton("Simulation",new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                m_navigationManager.simulate(m_route,60);
                m_map.setTilt(60);
                startForegroundService();
            };
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        m_navigationManager.setMapUpdateMode(NavigationManager.MapUpdateMode.ROADVIEW);

        addNavigationListeners();
    }

    private void addNavigationListeners() {

        m_navigationManager.addNavigationManagerEventListener(new WeakReference<NavigationManager.NavigationManagerEventListener>(m_navigationManagerEventListener));

        m_navigationManager.addPositionListener(new WeakReference<NavigationManager.PositionListener>(m_positionListener));
    }

    private NavigationManager.PositionListener m_positionListener = new NavigationManager.PositionListener() {
        @Override
        public void onPositionUpdated(GeoPosition geoPosition) {
            System.out.println("Position   "+geoPosition);
        }
    };

    private NavigationManager.NavigationManagerEventListener m_navigationManagerEventListener = new NavigationManager.NavigationManagerEventListener() {
        @Override
        public void onRunningStateChanged() {
            Toast.makeText(m_activity, "Running state changed", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNavigationModeChanged() {
            Toast.makeText(m_activity, "Navigation mode changed", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onEnded(NavigationManager.NavigationMode navigationMode) {
            Toast.makeText(m_activity, navigationMode + " was ended", Toast.LENGTH_SHORT).show();
            stopForegroundService();
        }

        @Override
        public void onMapUpdateModeChanged(NavigationManager.MapUpdateMode mapUpdateMode) {
            Toast.makeText(m_activity, "Map update mode is changed to " + mapUpdateMode, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRouteUpdated(Route route) {
            Toast.makeText(m_activity, "Route updated", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCountryInfo(String s, String s1) {
            Toast.makeText(m_activity, "Country info updated from " + s + " to " + s1, Toast.LENGTH_SHORT).show();
        }
    };

    public void onDestroy() {
        if (m_navigationManager != null) {
            stopForegroundService();
            m_navigationManager.stop();
        }
    }
}